/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_ALIGNMENT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_PARA;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_PRESENTATION;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_SHEET_VIEW;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_TABLE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_TEXT_BODY;

/**
 * Provides a markup component.
 */
interface MarkupComponent extends XMLEvents {

    static boolean isSheetViewStart(MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && LOCAL_SHEET_VIEW.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isAlignmentEmptyElement(MarkupComponent markupComponent) {
        return markupComponent instanceof EmptyElement
                && LOCAL_ALIGNMENT.equals(((EmptyElement) markupComponent).getName().getLocalPart());
    }

    static boolean isPresentationStart(MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && LOCAL_PRESENTATION.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isTableStart(MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && LOCAL_TABLE.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isTextBodyStart(MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && LOCAL_TEXT_BODY.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isParagraphStart(MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && LOCAL_PARA.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isWordStylesStart(final MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && WordStyleDefinitions.STYLES.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isWordStylesEnd(final MarkupComponent markupComponent) {
        return markupComponent instanceof End
                && WordStyleDefinitions.STYLES.equals(((End) markupComponent).getName().getLocalPart());
    }

    static boolean isWordDocumentDefaultsStart(final MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && WordStyleDefinitions.DOC_DEFAULTS.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isWordDocumentDefaultsEnd(final MarkupComponent markupComponent) {
        return markupComponent instanceof End
                && WordStyleDefinitions.DOC_DEFAULTS.equals(((End) markupComponent).getName().getLocalPart());
    }


    static boolean isWordParagraphPropertiesDefaultStart(final MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && WordStyleDefinition.DocumentDefaults.PPR_DEFAULT.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isWordRunPropertiesDefaultStart(final MarkupComponent markupComponent) {
        return markupComponent instanceof Start
                && WordStyleDefinition.DocumentDefaults.RPR_DEFAULT.equals(((Start) markupComponent).getName().getLocalPart());
    }

    static boolean isWordRunPropertiesDefaultEnd(final MarkupComponent markupComponent) {
        return markupComponent instanceof End
                && WordStyleDefinition.DocumentDefaults.RPR_DEFAULT.equals(((End) markupComponent).getName().getLocalPart());
    }

    static boolean isRunProperties(final MarkupComponent markupComponent) {
        return markupComponent instanceof RunProperties;
    }

    /**
     * Provides a start markup component.
     */
    class Start implements MarkupComponent, Nameable {
        private XMLEventFactory eventFactory;
        private StartElement startElement;
        private List<Attribute> attributes = new ArrayList<>();

        Start(XMLEventFactory eventFactory, StartElement startElement) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;

            Iterator iterator = startElement.getAttributes();

            while (iterator.hasNext()) {
                attributes.add((Attribute) iterator.next());
            }
        }

        @Override
        public List<XMLEvent> getEvents() {
            return Collections.singletonList((XMLEvent) eventFactory.createStartElement(startElement.getName(), getAttributes().iterator(), startElement.getNamespaces()));
        }

        @Override
        public QName getName() {
            return startElement.getName();
        }

        List<Attribute> getAttributes() {
            return attributes;
        }
    }

    /**
     * Provides an end markup component.
     */
    class End implements MarkupComponent, Nameable {
        private EndElement endElement;

        End(EndElement endElement) {
            this.endElement = endElement;
        }

        @Override
        public List<XMLEvent> getEvents() {
            return Collections.singletonList((XMLEvent) endElement);
        }

        @Override
        public QName getName() {
            return this.endElement.getName();
        }
    }

    /**
     * Provides an empty element markup component.
     */
    class EmptyElement implements MarkupComponent, Nameable {
        private static final int EMPTY_ELEMENT_EVENTS_SIZE = 2;

        private XMLEventFactory eventFactory;
        private StartElement startElement;
        private EndElement endElement;
        private List<Attribute> attributes = new ArrayList<>();

        EmptyElement(XMLEventFactory eventFactory, StartElement startElement, EndElement endElement) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;
            this.endElement = endElement;

            Iterator iterator = startElement.getAttributes();

            while (iterator.hasNext()) {
                attributes.add((Attribute) iterator.next());
            }
        }

        @Override
        public List<XMLEvent> getEvents() {
            List<XMLEvent> events = new ArrayList<>(EMPTY_ELEMENT_EVENTS_SIZE);

            events.add(eventFactory.createStartElement(startElement.getName(), getAttributes().iterator(), startElement.getNamespaces()));
            events.add(endElement);

            return events;
        }

        @Override
        public QName getName() {
            return startElement.getName();
        }

        List<Attribute> getAttributes() {
            return attributes;
        }
    }

    /**
     * Provides a general markup component.
     */
    class General implements MarkupComponent {
        private List<XMLEvent> events;

        General(List<XMLEvent> events) {
            this.events = events;
        }

        @Override
        public List<XMLEvent> getEvents() {
            return events;
        }
    }
}
