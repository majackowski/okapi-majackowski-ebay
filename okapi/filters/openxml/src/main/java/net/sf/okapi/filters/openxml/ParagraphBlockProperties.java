/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_VAL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;

interface ParagraphBlockProperties extends BlockProperties {
    String PPR = "pPr";

    boolean containsRunPropertyDeletedParagraphMark();
    String paragraphStyle();
    String highlightColor();
    String textColor();
    void refine(final QName innerBlockPropertyName, final String styleId, final List<RunProperty> commonRunProperties) throws XMLStreamException;

    class Drawing implements ParagraphBlockProperties {
        private static final String LOCAL_LEVEL = "lvl";

        private final Default defaultBlockProperties;
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final StrippableAttributes.General generalStrippableAttributes;
        private final SchemaDefinition.Component schemaDefinition;

        Drawing(
            final BlockProperties.Default defaultBlockProperties,
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final StrippableAttributes.General generalStrippableAttributes,
            final SchemaDefinition.Component schemaDefinition
        ) {
            this.defaultBlockProperties = defaultBlockProperties;
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.generalStrippableAttributes = generalStrippableAttributes;
            this.schemaDefinition = schemaDefinition;
        }

        @Override
        public List<XMLEvent> getEvents() {
            return this.defaultBlockProperties.getEvents();
        }

        @Override
        public QName getName() {
            return this.defaultBlockProperties.getName();
        }

        @Override
        public StartElement startElement() {
            return this.defaultBlockProperties.startElement();
        }

        @Override
        public List<Attribute> attributes() {
            return this.defaultBlockProperties.attributes();
        }

        @Override
        public List<BlockProperty> properties() {
            return this.defaultBlockProperties.properties();
        }

        @Override
        public boolean isEmpty() {
            return this.defaultBlockProperties.isEmpty();
        }

        @Override
        public boolean containsRunPropertyDeletedParagraphMark() {
            return false;
        }

        @Override
        public String paragraphStyle() {
            final Attribute paragraphLevelAttribute = paragraphLevelAttribute();

            if (null != paragraphLevelAttribute) {
                return paragraphLevelAttribute.getValue();
            }
            return null;
        }

        private Attribute paragraphLevelAttribute() {
            for (Attribute attribute : this.defaultBlockProperties.attributes()) {
                if (LOCAL_LEVEL.equals(attribute.getName().getLocalPart())) {
                    return attribute;
                }
            }
            return null;
        }

        @Override
        public String highlightColor() {
            return null;
        }

        @Override
        public String textColor() {
            return null;
        }

        @Override
        public void refine(final QName innerBlockPropertyName, final String styleId, final List<RunProperty> commonRunProperties) throws XMLStreamException {
            final ListIterator<BlockProperty> propertiesIterator = this.defaultBlockProperties.properties().listIterator();
            while (propertiesIterator.hasNext()) {
                final BlockProperty blockProperty = propertiesIterator.next();
                if (blockProperty.getName().equals(innerBlockPropertyName)) {
                    updateProperty(propertiesIterator, blockProperty, commonRunProperties);
                    return;
                }
            }
            addProperty(propertiesIterator, innerBlockPropertyName, commonRunProperties);
        }

        private void updateProperty(
            final ListIterator<BlockProperty> propertiesIterator,
            final BlockProperty blockProperty,
            final List<RunProperty> commonRunProperties
        ) throws XMLStreamException {
            final RunProperties runProperties = asRunProperties(blockProperty);
            runProperties.refine(commonRunProperties);
            runProperties.alignWith(this.schemaDefinition);
            propertiesIterator.set(
                BlockPropertyFactory.createBlockProperty(runProperties.getEvents())
            );
        }

        private RunProperties asRunProperties(final BlockProperty blockProperty) throws XMLStreamException {
            final XMLEventReader eventReader = new XMLEventsReader(blockProperty.getEvents());
            final StartElement startElement = eventReader.nextEvent().asStartElement();
            final StartElementContext initialStartElementContext = StartElementContextFactory.createStartElementContext(
                startElement,
                this.defaultBlockProperties.startElement(),
                eventReader,
                this.eventFactory,
                this.conditionalParameters,
                null
            );
            final StartElementContext startElementContext = StartElementContextFactory.createStartElementContext(
                this.generalStrippableAttributes.strip(initialStartElementContext),
                initialStartElementContext
            );
            final RunProperties runProperties = new RunPropertiesParser(
                startElementContext,
                new RunSkippableElements(startElementContext)
            )
            .parse();

            if (runProperties.getProperties().isEmpty()) {
                return new RunProperties.Default(
                    this.eventFactory,
                    blockProperty.getEvents().get(0).asStartElement(),
                    blockProperty.getEvents().get(blockProperty.getEvents().size() - 1).asEndElement(),
                    new ArrayList<>()
                );
            }
            return runProperties;
        }

        private void addProperty(
            final ListIterator<BlockProperty> propertiesIterator,
            final QName innerBlockPropertyName,
            final List<RunProperty> commonRunProperties
        ) {
            final List<Attribute> attributes = asAttributes(commonRunProperties);
            final List<RunProperty> properties = asProperties(commonRunProperties);
            final List<XMLEvent> events = new ArrayList<>();

            events.add(this.eventFactory.createStartElement(innerBlockPropertyName, attributes.iterator(), null));
            events.addAll(asXMLEvents(properties));
            events.add(this.eventFactory.createEndElement(innerBlockPropertyName, null));

            rewindToSchemaDefinedPlace(propertiesIterator, innerBlockPropertyName).add(
                BlockPropertyFactory.createBlockProperty(events)
            );
        }

        private List<Attribute> asAttributes(final List<RunProperty> commonRunProperties) {
            return commonRunProperties
                .stream()
                .filter(runProperty -> runProperty instanceof RunProperty.AttributeRunProperty)
                .map(runProperty -> this.eventFactory.createAttribute(runProperty.getName(), runProperty.getValue()))
                .collect(Collectors.toList());
        }

        private List<RunProperty> asProperties(final List<RunProperty> commonRunProperties) {
            return commonRunProperties
                .stream()
                .filter(runProperty -> !(runProperty instanceof RunProperty.AttributeRunProperty))
                .collect(Collectors.toList());
        }

        private static List<XMLEvent> asXMLEvents(final List<RunProperty> commonRunProperties) {
            return commonRunProperties
                .stream()
                .map(RunProperty::getEvents)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        }

        /**
         * Rewinds the block properties iterator to a schema defined place.
         *
         * It is assumed that the iterator does not have next element
         * (we are at the end of the properties list).
         *
         * @param propertiesIterator     The properties iterator
         * @param innerBlockPropertyName The inner block property name
         *
         * @return The block properties iterator
         */
        private ListIterator<BlockProperty> rewindToSchemaDefinedPlace(
            final ListIterator<BlockProperty> propertiesIterator,
            final QName innerBlockPropertyName
        ) {
            if (!propertiesIterator.hasPrevious()) {
                // empty properties, just returning
                return propertiesIterator;
            }
            final Iterator<SchemaDefinition.Component> iterator =
                    this.schemaDefinition.listIteratorAfter(innerBlockPropertyName);
            if (!iterator.hasNext()) {
                // the inner block property is the last component in the schema definition
                return propertiesIterator;
            }

            while (iterator.hasNext()) {
                final SchemaDefinition.Component component = iterator.next();
                while (propertiesIterator.hasPrevious()) {
                    final BlockProperty blockProperty = propertiesIterator.previous();
                    if (blockProperty.getName().equals(component.name())) {
                        return propertiesIterator;
                    }
                }
                rewindToEndOfProperties(propertiesIterator);
            }

            // no block properties present after the inner block property
            return propertiesIterator;
        }

        private void rewindToEndOfProperties(final ListIterator<BlockProperty> propertiesIterator) {
            while (propertiesIterator.hasNext()) {
                propertiesIterator.next();
            }
        }
    }

    class Word implements ParagraphBlockProperties {
        private static final String PARAGRAPH_STYLE = "pStyle";
        private static final String VAL = "val";

        private final Default defaultBlockProperties;
        private final XMLEventFactory eventFactory;

        public Word(final Default defaultBlockProperties, final XMLEventFactory eventFactory) {
            this.defaultBlockProperties = defaultBlockProperties;
            this.eventFactory = eventFactory;
        }

        @Override
        public List<XMLEvent> getEvents() {
            return this.defaultBlockProperties.getEvents();
        }

        @Override
        public QName getName() {
            return this.defaultBlockProperties.getName();
        }

        @Override
        public StartElement startElement() {
            return this.defaultBlockProperties.startElement();
        }

        @Override
        public List<Attribute> attributes() {
            return this.defaultBlockProperties.attributes();
        }

        @Override
        public List<BlockProperty> properties() {
            return this.defaultBlockProperties.properties();
        }

        @Override
        public boolean isEmpty() {
            return this.defaultBlockProperties.isEmpty();
        }

        @Override
        public boolean containsRunPropertyDeletedParagraphMark() {
            return this.defaultBlockProperties.properties()
                .stream()
                .filter(bp -> RunProperties.RPR.equals(bp.getName().getLocalPart()))
                .map(rp -> rp.getEvents())
                .flatMap(events -> events.stream())
                .filter(e -> e.isStartElement())
                .map(e -> e.asStartElement().getName())
                .anyMatch(n -> SkippableElement.RevisionProperty.RUN_PROPERTY_DELETED_PARAGRAPH_MARK.toName().equals(n));
        }

        @Override
        public String paragraphStyle() {
            BlockProperty paragraphStyleProperty = paragraphStyleProperty();

            if (null != paragraphStyleProperty) {
                return getAttributeValue(paragraphStyleProperty.getEvents().get(0).asStartElement(), WPML_VAL);
            }

            return null;
        }

        private BlockProperty paragraphStyleProperty() {
            return blockProperty(PARAGRAPH_STYLE);
        }

        private BlockProperty blockProperty(final String localPart) {
            final QName name = new QName(
                this.defaultBlockProperties.getName().getNamespaceURI(),
                localPart
            );
            for (BlockProperty property : this.defaultBlockProperties.properties()) {
                if (property.getName().equals(name)) {
                    return property;
                }
            }
            return null;
        }

        @Override
        public String highlightColor() {
            BlockProperty highlightColorProperty = highlightColorProperty();

            if (null != highlightColorProperty) {
                return getAttributeValue(highlightColorProperty.getEvents().get(0).asStartElement(), WPML_VAL);
            }
            return null;
        }

        private BlockProperty highlightColorProperty() {
            return blockProperty(RunProperty.RunHighlightProperty.NAME);
        }

        @Override
        public String textColor() {
            BlockProperty colorProperty = textColorProperty();

            if (null != colorProperty) {
                return getAttributeValue(colorProperty.getEvents().get(0).asStartElement(), WPML_VAL);
            }
            return null;
        }

        private BlockProperty textColorProperty() {
            return blockProperty(RunProperty.RunColorProperty.NAME);
        }

        @Override
        public void refine(final QName innerBlockPropertyName, final String styleId, final List<RunProperty> commonRunProperties) throws XMLStreamException {
            final ListIterator<BlockProperty> propertiesIterator = this.defaultBlockProperties.properties().listIterator();
            while (propertiesIterator.hasNext()) {
                final BlockProperty blockProperty = propertiesIterator.next();
                if (Word.PARAGRAPH_STYLE.equals(blockProperty.getName().getLocalPart())) {
                    updateProperty(propertiesIterator, innerBlockPropertyName, styleId);
                    return;
                }
            }
            addProperty(propertiesIterator, innerBlockPropertyName, styleId);
        }

        private void updateProperty(
            final ListIterator<BlockProperty> propertiesIterator,
            final QName innerBlockPropertyName,
            final String styleId
        ) {
            propertiesIterator.set(
                blockProperty(innerBlockPropertyName, styleId)
            );
        }

        private void addProperty(
            final ListIterator<BlockProperty> propertiesIterator,
            final QName innerBlockPropertyName,
            final String styleId
        ) {
            propertiesIterator.add(
                blockProperty(innerBlockPropertyName, styleId)
            );
        }

        private BlockProperty blockProperty(final QName innerBlockPropertyName, final String styleId) {
            return BlockPropertyFactory.createBlockProperty(
                new CreationalParameters(
                    this.eventFactory,
                    innerBlockPropertyName.getPrefix(),
                    innerBlockPropertyName.getNamespaceURI()
                ),
                Word.PARAGRAPH_STYLE,
                Collections.singletonMap(Word.VAL, styleId)
            );
        }
    }
}
