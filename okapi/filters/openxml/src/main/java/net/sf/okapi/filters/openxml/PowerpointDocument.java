/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static net.sf.okapi.filters.openxml.ContentTypes.Types.Common.CORE_PROPERTIES_TYPE;
import static net.sf.okapi.filters.openxml.ContentTypes.Types.Common.PACKAGE_RELATIONSHIPS;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;

import net.sf.okapi.common.Event;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Powerpoint;
import net.sf.okapi.filters.openxml.Relationships.Rel;

class PowerpointDocument implements Document {
	private static final String SLIDE_LAYOUT = "/slideLayout";
	private static final String COMMENTS = "/comments";
	private static final String NOTES_SLIDE = "/notesSlide";
	private static final String NOTES_MASTER = "/notesMaster";
	private static final String CHART = "/chart";
	private static final String DIAGRAM_DATA = "/diagramData";

	private static final Pattern RELS_NAME_PATTERN = Pattern.compile(".+slide\\d+\\.xml\\.rels");
	private Matcher relsNameMatcher = RELS_NAME_PATTERN.matcher("").reset();

	private final Document.General generalDocument;
	private Enumeration<? extends ZipEntry> entries;
	private StyleDefinitions presentationNotesStyleDefinitions;

	private List<String> slideNames = new ArrayList<>();
	private List<String> slideLayoutNames = new ArrayList<>();

	/**
	 * Uses the slide name as key and the comment name as value.
	 */
	private Map<String, String> slidesByComment = new HashMap<>();

	/**
	 * Uses the slide name as key and the note name as value.
	 */
	private Map<String, String> slidesByNote = new HashMap<>();

	/**
	 * Uses the slide name as key and the chart name as value.
	 */
	private Map<String, String> slidesByChart = new HashMap<>();

	/**
	 * Uses the slide name as key and the diagram name as value.
	 */
	private Map<String, String> slidesByDiagramData = new HashMap<>();

	PowerpointDocument(final Document.General generalDocument) {
		this.generalDocument = generalDocument;
	}

	@Override
	public Event open() throws IOException, XMLStreamException {
		slideNames = findSlides();
		this.entries = entries();
		initialisePresentationNotesStyleDefinitions();
		slideLayoutNames = findSlideLayouts(slideNames);
		slidesByComment = findComments(slideNames);
		slidesByNote = findNotes(slideNames);
		slidesByChart = findCharts(slideNames);
		slidesByDiagramData = findDiagramDatas(slideNames);

		return this.generalDocument.startDocumentEvent();
	}

	/**
	 * Do additional reordering of the entries for PPTX files to make
	 * sure that slides are parsed in the correct order.  This is done
	 * by scraping information from one of the rels files and the
	 * presentation itself in order to determine the proper order, rather
	 * than relying on the order in which things appeared in the zip.
	 * @return the sorted enum of ZipEntry
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	private Enumeration<? extends ZipEntry> entries() throws IOException, XMLStreamException {
		Enumeration<? extends ZipEntry> entries = this.generalDocument.entries();
		List<? extends ZipEntry> entryList = Collections.list(entries);
		entryList.sort(new ZipEntryComparator(reorderedPartNames()));
		return Collections.enumeration(entryList);
	}

	private List<String> reorderedPartNames() throws IOException, XMLStreamException {
		final List<String> names = new LinkedList<>();
		for (final String slideName : this.slideNames) {
			names.add(slideName);
			if (this.generalDocument.conditionalParameters().getReorderPowerpointNotesAndComments()) {
				final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
				names.addAll(slideRelationshipTargetsForType(slideName, namespaceUri.concat(NOTES_SLIDE)));
				names.addAll(slideRelationshipTargetsForType(slideName, namespaceUri.concat(COMMENTS)));
			}
		}
		return names;
	}

	private List<String> slideRelationshipTargetsForType(final String slideName, final String typeUri) throws IOException, XMLStreamException {
		List<Rel> rels = this.generalDocument.relationshipsForPartName(slideName).getRelByType(typeUri);
		return rels == null ? Collections.emptyList() :
				rels.stream()
						.map(r -> r.target)
						.collect(Collectors.toList());
	}

	private void initialisePresentationNotesStyleDefinitions() throws IOException, XMLStreamException {
		String relationshipTarget = this.generalDocument.relationshipTargetFor(
			this.generalDocument.documentRelationshipsNamespace().uri().concat(NOTES_MASTER)
		);

		if (null == relationshipTarget) {
			this.presentationNotesStyleDefinitions = new StyleDefinitions.Empty();
			return;
		}

		try (final Reader reader = this.generalDocument.getPartReader(relationshipTarget)) {
			this.presentationNotesStyleDefinitions = new PresentationNotesStyleDefinitions(
				this.generalDocument.conditionalParameters(),
				this.generalDocument.inputFactory(),
				this.generalDocument.eventFactory()
			);
			this.presentationNotesStyleDefinitions.readWith(reader);
		}
	}

	List<String> findSlides() throws IOException, XMLStreamException {
		// XXX Not strictly correct, I should really look for the main document and then go from there
		Relationships rels = this.generalDocument.getRelationships("ppt/_rels/presentation.xml.rels");
		Presentation pres = new Presentation(this.generalDocument.inputFactory(), rels);
		pres.parseFromXML(this.generalDocument.getPartReader("ppt/presentation.xml"));
		return pres.getSlidePartNames();
	}

	/**
	 * Examine relationship information to find all layouts that are used in
	 * a slide in this document.  Return a list of their entry names, in order.
	 * @return list of entry names.
	 * @throws XMLStreamException See {@link Document.General#relationshipsForPartName(String)}
	 * @throws IOException See {@link Document.General#relationshipsForPartName(String)}
	 */
	List<String> findSlideLayouts(List<String> slideNames) throws IOException, XMLStreamException {
		List<String> layouts = new ArrayList<>();
		final String typeUri = this.generalDocument.documentRelationshipsNamespace().uri().concat(SLIDE_LAYOUT);
		for (String slideName : slideNames) {
			List<Relationships.Rel> rels =
					this.generalDocument.relationshipsForPartName(slideName).getRelByType(typeUri);
			if (!rels.isEmpty()) {
				layouts.add(rels.get(0).target);
			}
		}
		return layouts;
	}

	/**
	 * Initializes the relationships of type {@link #COMMENTS}.
	 */
	private Map<String, String> findComments(List<String> slideNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.RelsByEntry(slideNames, namespaceUri.concat(COMMENTS));
	}

	/**
	 * Initializes the relationships of type {@link #NOTES_SLIDE}.
	 */
	private Map<String, String> findNotes(List<String> slideNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.RelsByEntry(slideNames, namespaceUri.concat(NOTES_SLIDE));
	}

	/**
	 * Initializes the relationships of type {@link #CHART}.
	 */
	private Map<String, String> findCharts(List<String> slideNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.RelsByEntry(slideNames, namespaceUri.concat(CHART));
	}

	/**
	 * Initializes the relationships of type {@link #DIAGRAM_DATA}.
	 */
	private Map<String, String> findDiagramDatas(List<String> slideNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.RelsByEntry(slideNames, namespaceUri.concat(DIAGRAM_DATA));
	}

	@Override
	public boolean isStyledTextPart(final ZipEntry entry) {
		final String type = this.generalDocument.contentTypeFor(entry);
		if (type.equals(Powerpoint.SLIDE_TYPE)) return true;
		if (type.equals(Drawing.DIAGRAM_TYPE)) return true;
		if (isMasterPart(entry.getName(), type)) return true;
		if (this.generalDocument.conditionalParameters().getTranslatePowerpointNotes() && type.equals(Powerpoint.NOTES_TYPE)) return true;
		if (type.equals(Drawing.CHART_TYPE)) return true;
		return false;
	}

	private boolean isMasterPart(String entryName, String type) {
		if (this.generalDocument.conditionalParameters().getTranslatePowerpointMasters()) {
			if (type.equals(Powerpoint.MASTERS_TYPE)) return true;
			// Layouts are translatable if we are translating masters and this particular layout is
			// in use by a slide
			if (type.equals(Powerpoint.LAYOUT_TYPE)
					&& slideLayoutNames.contains(entryName)) return true;
		}

		return false;
	}

	@Override
	public boolean hasPostponedTranslatables() {
		return false;
	}

	@Override
	public void updatePostponedTranslatables(final String key, final String value) {
	}

	@Override
	public boolean hasNextPart() {
		return this.entries.hasMoreElements();
	}

	@Override
	public Part nextPart() throws IOException, XMLStreamException {
		final ZipEntry entry = this.entries.nextElement();
		final String contentType = this.generalDocument.contentTypeFor(entry);

		relsNameMatcher.reset(entry.getName());
		if (isRelationshipsPart(contentType) && relsNameMatcher.matches() && this.generalDocument.conditionalParameters().getExtractExternalHyperlinks()) {
			return new RelationshipsPart(
					this.generalDocument,
					entry
			);
		}

		if (!isTranslatablePart(entry)) {
			if (isModifiablePart(contentType)) {
				return new ModifiablePart(this.generalDocument, entry, this.generalDocument.inputStreamFor(entry));
			}
			return new NonModifiablePart(this.generalDocument, entry);
		}

		final StyleDefinitions styleDefinitions;
		if (Powerpoint.NOTES_TYPE.equals(contentType)) {
			styleDefinitions = this.presentationNotesStyleDefinitions;
		} else {
			styleDefinitions = new StyleDefinitions.Empty();
		}

		if (isStyledTextPart(entry)) {
			final StyleOptimisation styleOptimisation = styleOptimisationsFor(entry, styleDefinitions);
			if (isMasterPart(entry.getName(), contentType)) {
				return new MasterPart(
					this.generalDocument,
					entry,
					styleDefinitions,
					styleOptimisation
				);
			}
			return new StyledTextPart(
				this.generalDocument,
				entry,
				styleDefinitions,
				styleOptimisation
			);
		}

		ContentFilter contentFilter = new ContentFilter(this.generalDocument.conditionalParameters(), entry.getName());
		ParseType parseType = getParseType(contentType);
		this.generalDocument.conditionalParameters().nFileType = parseType;

		contentFilter.setUpConfig(parseType);

		// Other configuration
		return new DefaultPart(this.generalDocument, entry, contentFilter);
	}

	private static boolean isRelationshipsPart(String contentType) {
		return PACKAGE_RELATIONSHIPS.equals(contentType);
	}

	private static boolean isModifiablePart(String contentType) {
		return Powerpoint.MAIN_DOCUMENT_TYPE.equals(contentType);
	}

	private StyleOptimisation styleOptimisationsFor(final ZipEntry entry, final StyleDefinitions styleDefinitions) throws IOException, XMLStreamException {
		final Namespace namespace = namespacesOf(entry).forPrefix(Namespace.PREFIX_A);
		if (null == namespace) {
			return new StyleOptimisation.Bypass();
		}
		return new StyleOptimisation.Default(
			new StyleOptimisation.Bypass(),
			this.generalDocument.conditionalParameters(),
			this.generalDocument.eventFactory(),
			new QName(namespace.uri(), ParagraphBlockProperties.PPR, namespace.prefix()),
			new QName(namespace.uri(), RunProperties.DEF_RPR, namespace.prefix()),
			Collections.emptyList(),
			styleDefinitions
		);
	}

	private Namespaces2 namespacesOf(final ZipEntry entry) throws IOException, XMLStreamException {
		try (final Reader reader = new InputStreamReader(this.generalDocument.inputStreamFor(entry))) {
			final Namespaces2 namespaces = new Namespaces2.Default(
				this.generalDocument.inputFactory()
			);
			namespaces.readWith(reader);
			return namespaces;
		}
	}

    private ParseType getParseType(String contentType) {
        ParseType parseType;
		if (contentType.equals(CORE_PROPERTIES_TYPE)) {
			parseType = ParseType.MSWORDDOCPROPERTIES;
		}
		else if (contentType.equals(Powerpoint.COMMENTS_TYPE)) {
			parseType = ParseType.MSPOWERPOINTCOMMENTS;
		}
		else {
			throw new IllegalStateException("Unexpected content type " + contentType);
		}

		return parseType;
	}

	private boolean isTranslatablePart(final ZipEntry entry) throws IOException, XMLStreamException {
		final String type = this.generalDocument.contentTypeFor(entry);
		if (!entry.getName().endsWith(".xml")) return false;
        if (isExcluded(entry.getName(), type)) return false;
		if (isHidden(entry.getName(), type)) return false;
		if (isStyledTextPart(entry)) return true;
		if (this.generalDocument.conditionalParameters().getTranslateDocProperties() && type.equals(CORE_PROPERTIES_TYPE)) return true;
		if (this.generalDocument.conditionalParameters().getTranslateComments() && type.equals(Powerpoint.COMMENTS_TYPE)) return true;
		if (type.equals(Drawing.DIAGRAM_TYPE)) return true;
		if (this.generalDocument.conditionalParameters().getTranslatePowerpointNotes() && type.equals(Powerpoint.NOTES_TYPE)) return true;
		return false;
	}

	/**
	 * @param entryName ZIP entry name
	 * @param contentType the entry's content type
	 * @return {@code true} if the entry is to be excluded due to
	 * {@link ConditionalParameters#getPowerpointIncludedSlideNumbersOnly()} and
	 * {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcluded(String entryName, String contentType) {
		return isExcludedSlide(entryName, contentType)
				|| isExcludedNote(entryName, contentType)
				|| isExcludedComment(entryName, contentType)
				|| isExcludedChart(entryName, contentType)
				|| isExcludedDiagramData(entryName, contentType);
	}

	/**
	 * @param entryName the entry name
	 * @param contentType the entry's content type
	 * @return {@code true} if the given entry represents a slide that was not included using
	 * option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedSlide(String entryName, String contentType) {
		if (!Powerpoint.SLIDE_TYPE.equals(contentType)) {
			return false;
		}

		if (!this.generalDocument.conditionalParameters().getPowerpointIncludedSlideNumbersOnly()) {
			return false;
		}

		int slideIndex = slideNames.indexOf(entryName);
		if (slideIndex == -1) {
			return false;
		}

		int slideNumber = slideIndex + 1; // human readable / 1-based slide numbers
		return !this.generalDocument.conditionalParameters().tsPowerpointIncludedSlideNumbers.contains(slideNumber);
	}

	/**
	 * @param entryName the entry name
	 * @param contentType the entry's content type
	 * @return {@code true} if the given entry represents a note that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedNote(String entryName, String contentType) {
		if (!Powerpoint.NOTES_TYPE.equals(contentType)
				|| !slidesByNote.containsKey(entryName)) {
			return false;
		}

		String slideName = slidesByNote.get(entryName);
		return isExcludedSlide(slideName, Powerpoint.SLIDE_TYPE);
	}

	/**
	 * @param entryName the entry name
	 * @param contentType the entry's content type
	 * @return {@code true} if the given entry represents a comment that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedComment(String entryName, String contentType) {
		if (!Powerpoint.COMMENTS_TYPE.equals(contentType)
				|| !slidesByComment.containsKey(entryName)) {
			return false;
		}

		String slideName = slidesByComment.get(entryName);
		return isExcludedSlide(slideName, Powerpoint.SLIDE_TYPE);
	}

	/**
	 * @param entryName the entry name
	 * @param contentType the entry's content type
	 * @return {@code true} if the given entry represents a chart that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedChart(String entryName, String contentType) {
		if (!Drawing.CHART_TYPE.equals(contentType)
				|| !slidesByChart.containsKey(entryName)) {
			return false;
		}

		String slideName = slidesByChart.get(entryName);
		return isExcludedSlide(slideName, Powerpoint.SLIDE_TYPE);
	}

	/**
	 * "Diagram data" is used by SmartArt, for example.
	 *
	 * @param entryName the entry name
	 * @param contentType the entry's content type
	 * @return {@code true} if the given entry represents a diagram that is used on a slide that was
	 * not included using option {@link ConditionalParameters#tsPowerpointIncludedSlideNumbers}
	 */
	private boolean isExcludedDiagramData(String entryName, String contentType) {
		if (!Drawing.DIAGRAM_TYPE.equals(contentType)
				|| !slidesByDiagramData.containsKey(entryName)) {
			return false;
		}

		String slideName = slidesByDiagramData.get(entryName);
		return isExcludedSlide(slideName, Powerpoint.SLIDE_TYPE);
	}

	private boolean isHidden(String entryName, String type) throws IOException, XMLStreamException {
		if (!this.generalDocument.conditionalParameters().getTranslatePowerpointHidden() && Powerpoint.SLIDE_TYPE.equals(type)) {
			return isHiddenSlide(entryName);
		}
		return false;
	}

	private boolean isHiddenSlide(String entryName) throws IOException, XMLStreamException {
		XMLEventReader eventReader = null;
		try {
			eventReader = this.generalDocument.inputFactory().createXMLEventReader(this.generalDocument.getPartReader(entryName));
			return PresentationSlide.fromXMLEventReader(eventReader).isHidden();
		} finally {
			if (null != eventReader) {
				eventReader.close();
			}
		}
	}

	@Override
	public void close() throws IOException {
	}
}
