/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import java.util.ListIterator;


/**
 * Provides a bidirectionality clarifier.
 */
class BidirectionalityClarifier {

    private final ConditionalParameters conditionalParameters;
    private final CreationalParameters creationalParameters;
    private final ClarificationParameters clarificationParameters;

    /**
     * Constructs the bidirectionality clarifier.
     *
     * @param conditionalParameters   Conditional parameters
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     */
    BidirectionalityClarifier(
        ConditionalParameters conditionalParameters,
        CreationalParameters creationalParameters,
        ClarificationParameters clarificationParameters
    ) {
        this.conditionalParameters = conditionalParameters;
        this.creationalParameters = creationalParameters;
        this.clarificationParameters = clarificationParameters;
    }

    /**
     * Clarifies a markup.
     */
    void clarifyMarkup(Markup markup) {
        ListIterator<MarkupComponent> iterator = markup.components().listIterator();

        while (iterator.hasNext()) {
            MarkupComponent component = iterator.next();

            if (MarkupComponent.isSheetViewStart(component)) {
                clarifySheetView(component);
            } else if (MarkupComponent.isAlignmentEmptyElement(component)) {
                clarifyAlignment(component);
            } else if (MarkupComponent.isPresentationStart(component)) {
                clarifyPresentation(component);
            } else if (MarkupComponent.isTableStart(component)) {
                clarifyTableProperties(iterator);
            } else if (MarkupComponent.isTextBodyStart(component)) {
                clarifyTextBodyProperties(iterator);
            } else if (MarkupComponent.isParagraphStart(component)) {
                clarifyParagraphProperties(iterator);
            } else if (MarkupComponent.isWordStylesStart(component)) {
                clarifyWordStyles(iterator);
            }
        }
    }

    private void clarifySheetView(MarkupComponent markupComponent) {
        new MarkupComponentClarifier.SheetViewClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponent);
    }

    private void clarifyAlignment(MarkupComponent markupComponent) {
        new MarkupComponentClarifier.AlignmentClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponent);
    }

    private void clarifyPresentation(MarkupComponent markupComponent) {
        new MarkupComponentClarifier.PresentationClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponent);
    }

    private void clarifyTableProperties(ListIterator<MarkupComponent> markupComponentIterator) {
        new BlockPropertiesClarifier.TablePropertiesClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponentIterator);
    }

    private void clarifyTextBodyProperties(ListIterator<MarkupComponent> markupComponentIterator) {
        new BlockPropertiesClarifier.TextBodyPropertiesClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponentIterator);
    }

    private void clarifyParagraphProperties(ListIterator<MarkupComponent> markupComponentIterator) {
        new BlockPropertiesClarifier.ParagraphPropertiesClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponentIterator);
    }


    private void clarifyWordStyles(final ListIterator<MarkupComponent> iterator) {
        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();

            if (MarkupComponent.isWordDocumentDefaultsStart(component)) {
                clarifyWordDocumentDefaults(iterator);
            } else if (MarkupComponent.isWordStylesEnd(component)) {
                break;
            }
            // todo handle the case when the document defaults are absent - make a specific clarifier
        }
    }

    private void clarifyWordDocumentDefaults(final ListIterator<MarkupComponent> iterator) {
        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();

            if (MarkupComponent.isWordParagraphPropertiesDefaultStart(component)) {
                clarifyParagraphProperties(iterator);
            } else if (MarkupComponent.isWordRunPropertiesDefaultStart(component)) {
                clarifyDefaultRunProperties(iterator);
            } else if (MarkupComponent.isWordDocumentDefaultsEnd(component)) {
                break;
            }
            // todo handle the case when the document default paragraph properties are absent - make a specific clarifier
            // todo handle the case when the document default run properties are absent - make a specific clarifier
        }
    }

    private void clarifyDefaultRunProperties(final ListIterator<MarkupComponent> iterator) {
        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();

            if (MarkupComponent.isRunProperties(component)) {
                iterator.set(clarifyRunProperties((RunProperties) component));
            } else if (MarkupComponent.isWordRunPropertiesDefaultEnd(component)) {
                break;
            }
        }
    }

    /**
     * Clarifies run properties.
     *
     * @param runProperties Run properties
     *
     * @return Clarified run properties
     *
     * {@code null} if the resulted run properties are empty
     */
    RunProperties clarifyRunProperties(RunProperties runProperties) {
        return new RunPropertiesClarifier(creationalParameters, clarificationParameters)
                .clarify(runProperties);
    }
}
